---
layout: home
title:  Home
section: home
position: 1
---

cats01 is Blah blah blah

## Installation

cats01 is available on scala 2.11, 2.12, and scalajs.

Add the following to your build.sbt
```scala
libraryDependencies ++= Seq(

  "com.livejournal.voylaf" %% "cats01-core" % "0.0.1")
```
