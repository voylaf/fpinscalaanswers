package com.livejournal.voylaf.tests

import com.livejournal.voylaf.REx8PropertyBasedTesting.{Gen, Prop, boolean, listOf1}
import com.livejournal.voylaf.redBook.Monoid

object MonoidTesting extends App {

  import Monoid._

  val propMonoidLawsBooleanAnd = monoidLaws(booleanAnd, boolean)
  Prop.run(propMonoidLawsBooleanAnd)
  //(SimpleRNG(24L))
  val propMonoidLawsIntSum = monoidLaws(intAddition, Gen.choose(0, 1000))
  Prop.run(propMonoidLawsIntSum)
  val propMonoidLawsIntMulti = monoidLaws(intMultiplication, Gen.choose(0, 1000))
  Prop.run(propMonoidLawsIntMulti)

  val prop1 = Prop.forAll(listOf1(Gen.choose(1, 100))) { ns =>
    val sorted = ns.sorted
    if (ns == sorted) isOrdered(ns.toVector)
    else !isOrdered(ns.toVector)
  }

  Prop.run(prop1)

  //val wcLaws = monoidLaws(wcMonoid, Gen.string)

}
