package com.livejournal.voylaf.redBook

import com.livejournal.voylaf.REx8PropertyBasedTesting.Gen
import com.livejournal.voylaf.REx7Parallelism.Par
import com.livejournal.voylaf.REx6PurelyFState.State

import language.higherKinds

trait Functor[F[_]] {
  def map[A, B](fa: F[A])(f: A => B): F[B]
}

trait Monad[F[_]] extends Functor[F] with Applicative[F] {
  def unit[A](a: => A): F[A]

  def flatMapViaCompose[A, B](ma: F[A])(f: A => F[B]): F[B] = {
    val g: Unit => F[A] = Unit => ma
    compose(g, f)(())
  }

  def flatMap[A, B](ma: F[A])(f: A => F[B]): F[B]

  override def map[A, B](ma: F[A])(f: A => B): F[B] = flatMap(ma)(a => unit(f(a)))

  def map2[A, B, C](ma: F[A], mb: F[B])(f: (A, B) => C): F[C] =
    flatMap(ma)(a => map(mb)(b => f(a, b)))

  override def sequence[A](lma: List[F[A]]): F[List[A]] = traverse(lma)(x => x)

  override def traverse[A, B](la: List[A])(f: A => F[B]): F[List[B]] =
    la.foldLeft(unit(List[B]()))((flb, a) => map2(f(a), flb)(_ :: _))

  override def replicateM[A](n: Int, ma: F[A]): F[List[A]] = {
    val list = List.fill(n)(ma)
    sequence(list)
  }

  override def product[A, B](ma: F[A], mb: F[B]): F[(A, B)] = map2(ma, mb)((_, _))

  def filterMm[A](ms: List[A])(f: A => F[Boolean]): F[List[A]] = {
    val g: A => F[Option[A]] = (a: A) => map(f(a))(b => if (b) Some(a) else None)
    val opt = traverse(ms)(a => g(a))
    map(opt)(list => list.flatten)
  }

  def filterM[A](ms: List[A])(f: A => F[Boolean]): F[List[A]] = ms match {
    case Nil => unit(Nil)
    case x :: xs =>
      flatMap(f(x)) { b =>
        if (!b) filterM(xs)(f)
        else map(filterM(xs)(f))(l => x :: l)
      }
  }

  //doesn't work by definition
//  def compose[G[_]](G: Monad[G]): Monad[({type f[x] = F[G[x]]})#f] = {
//    val self = this
//    new Monad[({type f[x] = F[G[x]]})#f] {
//      def unit[A](a: => A): F[G[A]] = self.unit(G.unit(a))
//
//      def flatMap[A, B](ma: F[G[A]])(f: A => F[G[B]]): F[G[B]] = {
//        self.flatMap(ma)(ga => G.flatMap(ga)(a => f(a)))
//      }
//    }
//  }

  def compose[A, B, C](f: A => F[B], g: B => F[C]): A => F[C] = a => flatMap(f(a))(g)

  def join[A](mma: F[F[A]]): F[A] = flatMap(mma)(ma => ma)

  def composeViaJoin[A, B, C](f: A => F[B], g: B => F[C]): A => F[C] = a => join(map(f(a))(b => g(b)))

}

case class Id[A](value: A) {
  def map[B](f: A => B) = Id(f(value))

  def flatMap[B](f: A => Id[B]): Id[B] = f(value)
}

/*
11.9
flatMap(flatMap(x)(f))(g) == flatMap(x)(a => flatMap(f(a))(g))
compose(compose(f, g), h) == compose(f, compose(g, h))

//compose(f, g) == flatMap(f(x))(g)
//compose(f, g) == a => flatMap(f(a))(g)

compose(k, h) == compose(f, a => flatMap(g(a))(h))
flatMap(flatMap(f(a))(g))(h) == flatMap(f(a))(b => flatMap(g(b))(h))
f(a) = x, g = f', h = g', b = a'
flatMap(flatMap(x)(f'))(g') == flatMap(x)(a' => flatMap(f'(a'))(g'))
***
* 11.10
* compose(f, unit) == f
* compose(unit, f) == f
* flatMap(x)(unit) == x
* flatMap(unit(y))(f) == f(y)
*
* f(a) == x
* compose(f, unit) == flatMap(f(a))(unit) == flatMap(x)(unit) == f(a) == x
* compose(unit, f) == flatMap(unit(y))(f) == f(y)
*
* 11.11 for Stream
* f = a => Stream[B]
* unit = a => Stream(a)
*
* x = Stream(z)
* Stream(z).flatMap(unit) == Stream(z).flatMap(Stream(_)) == Stream(z) == x
* unit(z).flatMap(f) == Stream(z).flatMap(f) == f(z)
*
* x.flatMap(f) == x.map(f).join
 */

object Monad {
  val genMonad = new Monad[Gen] {
    def unit[A](a: => A): Gen[A] = Gen.unit(a)

    def flatMap[A, B](ma: Gen[A])(f: A => Gen[B]): Gen[B] =
      ma flatMap f
  }

  val listMonad = new Monad[List] {
    def unit[A](a: => A): List[A] = List(a)

    def flatMap[A, B](ma: List[A])(f: A => List[B]): List[B] =
      ma flatMap f
  }

  val parMonad = new Monad[Par] {
    def unit[A](a: => A): Par[A] = Par.unit(a)

    def flatMap[A, B](ma: Par[A])(f: A => Par[B]): Par[B] =
      Par.flatMap(ma)(f)
  }

  val optMonad = new Monad[Option] {
    def unit[A](a: => A): Option[A] = Some(a)

    def flatMap[A, B](ma: Option[A])(f: A => Option[B]): Option[B] =
      ma.flatMap(f)
  }

  val streamMonad = new Monad[Stream] {
    def unit[A](a: => A): Stream[A] = Stream(a)

    def flatMap[A, B](ma: Stream[A])(f: A => Stream[B]): Stream[B] =
      ma.flatMap(f)
  }

  val idMonad = new Monad[Id] {
    def unit[A](a: => A) = Id(a)

    def flatMap[A, B](ma: Id[A])(f: A => Id[B]): Id[B] = ma.flatMap(f)
  }

  def stateMonad[S] = new Monad[({type f[x] = State[S, x]})#f] {
    def unit[A](a: => A): State[S, A] = State(s => (a, s))

    def flatMap[A, B](st: State[S, A])(f: A => State[S, B]): State[S, B] =
      st flatMap f
  }

  def getState[S]: State[S, S] = ???

  def setState[S](s: => S): State[S, Unit] = ???

  val F = stateMonad[Int]

  def zipWithIndex[A](as: List[A]): List[(Int, A)] =
    as.foldLeft(F.unit(List[(Int, A)]()))((acc, a) => for {
      xs <- acc
      n <- getState
      _ <- setState(n + 1)
    } yield (n, a) :: xs).run(0)._1.reverse

  case class Reader[R, A](run: R => A)

  object Reader {
    def readerMonad[R] = new Monad[({type f[x] = Reader[R, x]})#f] {
      def unit[A](a: => A): Reader[R, A] = Reader(r => a)

      def flatMap[A, B](st: Reader[R, A])(f: A => Reader[R, B]): Reader[R, B] = Reader { r =>
        f(st.run(r)).run(r)
      }
    }
  }

}
