package com.livejournal.voylaf.redBook

import language.higherKinds

trait Applicative[F[_]] extends Functor[F] {
  // primitive combinators
  def map2[A, B, C](fa: F[A], fb: F[B])(f: (A, B) => C): F[C]

  def unit[A](a: => A): F[A]

  def apply[A, B](fab: F[A => B])(fa: F[A]): F[B] =
    map2(fab, fa)((ab, a) => ab(a))

  // derived combinators
  def map[A, B](fa: F[A])(f: A => B): F[B] =
    map2(fa, unit(()))((a, _) => f(a))

  def mapViaApply[A, B](fa: F[A])(f: A => B): F[B] = apply(unit(f))(fa)

  def map2ViaApply[A, B, C](fa: F[A], fb: F[B])(f: (A, B) => C): F[C] = {
    val fbc = apply(unit(f.curried))(fa)
    apply(fbc)(fb)
  }

  def traverse[A, B](as: List[A])(f: A => F[B]): F[List[B]] =
    as.foldRight(unit(List[B]()))((a, fbs) => map2(f(a), fbs)(_ :: _))

  def sequence[A](fas: List[F[A]]): F[List[A]] = traverse(fas)(fa => fa)

  def replicateM[A](n: Int, fa: F[A]): F[List[A]] = sequence(List.fill(n)(fa))

  def product[A, B](fa: F[A], fb: F[B]): F[(A, B)] = map2(fa, fb)((_, _))

  def map3[A, B, C, D](fa: F[A],
                       fb: F[B],
                       fc: F[C])(f: (A, B, C) => D): F[D] = {
    val fbc = apply(unit(f.curried))(fa)
    val fcd = apply(fbc)(fb)
    apply(fcd)(fc)
  }

  def map4[A, B, C, D, E](fa: F[A],
                          fb: F[B],
                          fc: F[C],
                          fd: F[D])(f: (A, B, C, D) => E): F[E] = {
    val fbc = apply(unit(f.curried))(fa)
    val fcd = apply(fbc)(fb)
    val fde = apply(fcd)(fc)
    apply(fde)(fd)
  }

  def compose[G[_]](G: Applicative[G]): Applicative[({type f[x] = F[G[x]]})#f] = {
    val self = this
    new Applicative[({type f[x] = F[G[x]]})#f] {
      def unit[A](a: => A): F[G[A]] = self.unit(G.unit(a))

      def map2[A, B, C](fa: F[G[A]], fb: F[G[B]])(f: (A, B) => C): F[G[C]] = {
        self.map2(fa, fb)(G.map2(_, _)(f))
      }
    }
  }

  def product[G[_]](G: Applicative[G]): Applicative[({type f[x] = (F[x], G[x])})#f] = {
    val self = this
    new Applicative[({type f[x] = (F[x], G[x])})#f] {
      def unit[A](a: => A): (F[A], G[A]) = (self.unit(a), G.unit(a))

      def map2[A, B, C](fa: (F[A], G[A]), fb: (F[B], G[B]))(f: (A, B) => C): (F[C], G[C]) = {
        val (faa, gaa) = fa
        val (fbb, gbb) = fb
        (self.map2(faa, fbb)(f), G.map2(gaa, gbb)(f))
      }
    }
  }

  def sequenceMap[K, V](ofa: Map[K, F[V]]): F[Map[K, V]] =
    ofa.foldLeft(unit(Map[K, V]())){case (acc, (k, fv)) => map2(acc, fv)((a, v) => a + (k -> v))}
}

object Applicative {

  val streamApplicative = new Applicative[Stream] {
    def unit[A](a: => A): Stream[A] =
      Stream.continually(a)

    def map2[A, B, C](a: Stream[A], b: Stream[B])(
      f: (A, B) => C): Stream[C] =
      a zip b map f.tupled

    //override def sequence[A](a: List[Stream[A]]): Stream[List[A]] = ???
  }

  def eitherMonad[E]: Monad[({type f[x] = Either[E, x]})#f] = new Monad[({type f[x] = scala.Either[E, x]})#f] {
    def unit[A](a: => A): Either[E, A] = Right(a)

    def flatMap[A, B](ma: Either[E, A])(f: A => Either[E, B]): Either[E, B] =
      ma.flatMap(f)
  }

  //doesn't work
  sealed trait Validation[+E, +A]

  case class Failure[E](head: E, tail: Vector[E] = Vector()) extends Validation[E, Nothing]

  case class Success[A](a: A) extends Validation[Nothing, A]

  //    def validationApplicative[E]: Applicative[({type f[x] = Validation[E,x]})#f] =
  //      new Applicative[({type f[x] = Validation[E,x]})#f] {
  //        override def map2[A, B, C](fa: Validation[E, A], fb: Validation[E, B])(f: (A, B) => C): Validation[E, C] =
  //          (fa, fb) match {
  //            case (Success(a), Success(b)) => Success(f(a, b))
  //            case (Failure(h1, t1), Failure(h2, t2)) => Failure(h1, t1 ++ Vector(h2) ++ t2)
  //            case (Failure(head, tail), _) => Failure(head, tail)
  //            case (_, Failure(head, tail)) => Failure(head, tail)
  //        }
  //
  //        def unit[A](a: => A): Validation[E, A] = Success(a)
  //      }

  def assoc[A, B, C](p: (A, (B, C))): ((A, B), C) = p match {
    case (a, (b, c)) => ((a, b), c)
  }

  def productF[I, O, I2, O2](f: I => O, g: I2 => O2): (I, I2) => (O, O2) = (i, i2) => (f(i), g(i2))

  /*
    * Laws
    1. Identity
    map2(unit(()), fa)((_,a) => a) == fa
    map2(fa, unit(()))((a,_) => a) == fa

    2. Associativity
    product(product(fa,fb),fc) == map(product(fa, product(fb,fc)))(assoc)

    3. Naturality of product
    map2(a,b)(productF(f,g)) == product(map(a)(f), map(b)(g))

   */

  /*
  12.7
  def map2[A, B, C](ma: F[A], mb: F[B])(f: (A, B) => C): F[C] = flatMap(ma)(a => map(mb)(b => f(a, b)))
  1.
  map2(unit(()), fa)((_,a) => a) == fa
  map2(fa, unit(()))((a,_) => a) == fa

  flatMap(unit(()))(a => map(fa)(b => (_, b) => b) == a => map(fa)(b => b) == fa
  flatMap(fa)(a => map(unit(())(b => (a, _) => a)) == flatMap(fa)(a => a) == fa
   */


  //  def product2[G[_]](G: Applicative[G]): Applicative[({type f[x] = (F[x], G[x])})#f] = {
  //    val self = this
  //    new Applicative[({type f[x] = (F[x], G[x])})#f] {
  //      def unit[A](a: => A) = (self.unit(a), G.unit(a))
  //
  //      override def apply[A, B](fs: (F[A => B], G[A => B]))(p: (F[A], G[A])) =
  //        (self.apply(fs._1)(p._1), G.apply(fs._2)(p._2))
  //    }
  //  }


}
