package com.livejournal.voylaf.redBook

import com.livejournal.voylaf.REx7Parallelism.Par

import language.higherKinds

trait ExternalEffects {

}

sealed trait Free[F[_], A] {
  def map[B](f: A => B): Free[F, B] = flatMap(f andThen (Return(_)))

  def flatMap[B](f: A => Free[F, B]): Free[F, B] = FlatMap(this, f)
}

case class Return[F[_], A](a: A) extends Free[F, A]

case class Suspend[F[_], A](s: F[A]) extends Free[F, A]

case class FlatMap[F[_], A, B](s: Free[F, A], f: A => Free[F, B]) extends Free[F, B]

object Free {

  def freeMonad[F[_]]: Monad[({type f[a] = Free[F, a]})#f] =
    new Monad[({type f[a] = Free[F, a]})#f] {
      def unit[A](a: => A): Free[F, A] = Return(a)

      def flatMap[A, B](ma: Free[F, A])(f: A => Free[F, B]): Free[F, B] = ma.flatMap(f)
    }

  @annotation.tailrec
  def runTrampoline[A](fa: Free[Function0, A]): A = fa match {
    case Return(a) => a
    case Suspend(s) => s()
    case FlatMap(s, f) => s match {
      case Return(a) => runTrampoline(f(a))
      case Suspend(s) => runTrampoline(f(s()))
      case FlatMap(s, g) => runTrampoline(s flatMap (a => g(a).flatMap(f)))
    }
  }

  def run[F[_], A](a: Free[F, A])(implicit G: Monad[F]): F[A] = step(a) match {
    case Return(a) => G.unit(a)
    case Suspend(r) => r
    case FlatMap(x, f) => x match {
      case Suspend(r) => G.flatMap(r)(a => run(f(a)))
      case _ => sys.error("Impossible; `step` eliminates these cases")
    }
  }

  @annotation.tailrec
  def step[F[_], A](free: Free[F, A]): Free[F, A] = free match {
    case FlatMap(FlatMap(x, f), g) => step(x flatMap (a => f(a) flatMap g))
    case FlatMap(Return(x), f) => step(f(x))
    case _ => free
  }

  sealed trait Console[A] {
    def toPar: Par[A]

    def toThunk: () => A
  }

  case object ReadLine extends Console[Option[String]] {
    def toPar = Par.lazyUnit(run)

    def toThunk = () => run

    def run: Option[String] =
      try Some(scala.io.StdIn.readLine())
      catch {
        case e: Exception => None
      }
  }

  case class PrintLine(line: String) extends Console[Unit] {
    def toPar = Par.lazyUnit(println(line))

    def toThunk = () => println(line)
  }

  object Console {
    type ConsoleIO[A] = Free[Console, A]

    def readLn: ConsoleIO[Option[String]] =
      Suspend(ReadLine)

    def printLn(line: String): ConsoleIO[Unit] =
      Suspend(PrintLine(line))
  }

  trait Translate[F[_], G[_]] {
    def apply[A](f: F[A]): G[A]
  }

  type ~>[F[_], G[_]] = Translate[F, G]

  val consoleToFunction0 =
    new (Console ~> Function0) {
      def apply[A](a: Console[A]) = a.toThunk
    }
  val consoleToPar =
    new (Console ~> Par) {
      def apply[A](a: Console[A]) = a.toPar
    }

  def runFree[F[_], G[_], A](free: Free[F, A])(t: F ~> G)(
    implicit G: Monad[G]): G[A] =
    step(free) match {
      case Return(a) => G.unit(a)
      case Suspend(r) => t(r)
      case FlatMap(Suspend(r), f) => G.flatMap(t(r))(a => runFree(f(a))(t))
      case _ => sys.error("Impossible; `step` eliminates these cases")
    }

  def translate[F[_], G[_], A](f: Free[F, A])(fg: F ~> G): Free[G, A] = ???

  def runConsole[A](a: Free[Console, A]): A = ???

}

