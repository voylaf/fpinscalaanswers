package com.livejournal.voylaf.redBook

import com.livejournal.voylaf.REx8PropertyBasedTesting._
import com.livejournal.voylaf.REx7Parallelism._

import language.higherKinds
import scala.collection.immutable.Stream.Empty

trait Monoid[A] {
  def op(a1: A, a2: A): A

  def zero: A
}

object Monoid {
  def intAddition: Monoid[Int] = new Monoid[Int] {
    def op(x: Int, y: Int) = x + y

    def zero = 0
  }

  def intMultiplication: Monoid[Int] = new Monoid[Int] {
    def op(x: Int, y: Int) = x * y

    def zero = 1
  }

  def booleanOr: Monoid[Boolean] = new Monoid[Boolean] {
    def op(a: Boolean, b: Boolean) = a || b

    def zero = false
  }

  def booleanAnd: Monoid[Boolean] = new Monoid[Boolean] {
    def op(a: Boolean, b: Boolean) = a && b

    def zero = true
  }

  def optionMonoid[A]: Monoid[Option[A]] = new Monoid[Option[A]] {
    def op(a: Option[A], b: Option[A]) = a.orElse(b)

    def zero = None
  }

  def endoMonoid[A]: Monoid[A => A] = new Monoid[A => A] {
    def op(f: A => A, g: A => A) = f compose g

    def zero = (a: A) => a
  }

  def monoidLaws[A](m: Monoid[A], gen: Gen[A]): Prop =
    zeroLaw(m, gen) && associativityLaw(m, listOf(gen)(3))

  def zeroLaw[A](m: Monoid[A], gen: Gen[A]): Prop = Prop.forAll(gen)(a => m.op(m.zero, a) == a)

  def associativityLaw[A](m: Monoid[A], gen: Gen[List[A]]): Prop =
    Prop.forAll(gen) { case List(a, b, c) => m.op(m.op(a, b), c) == m.op(a, m.op(b, c)) }

  def concatenate[A](as: List[A], m: Monoid[A]): A =
    as.foldLeft(m.zero)(m.op)

  def foldMap[A, B](as: List[A], m: Monoid[B])(f: A => B): B =
    as.foldLeft(m.zero)((b: B, a: A) => m.op(b, f(a)))

  def foldRight[A, B](as: List[A], m: Monoid[B])(f: (A, B) => B) = ???

  def foldMapV[A, B](v: IndexedSeq[A], m: Monoid[B])(f: A => B): B = v match {
    case s if s.isEmpty => m.zero
    case s if s.length == 1 => f(s(0))
    case _ =>
      val (left, right) = v.splitAt(v.length / 2)
      m.op(foldMapV(left, m)(f), foldMapV(right, m)(f))
  }

  def par[A](m: Monoid[A]): Monoid[Par[A]] = new Monoid[Par[A]] {
    def op(a1: Par[A], a2: Par[A]): Par[A] = Par.map2(a1, a2)(m.op)

    def zero: Par[A] = Par.unit(m.zero)
  }

  def parFoldMapZ[A, B](v: IndexedSeq[A], m: Monoid[B])(f: A => B): Par[B] = {
    val parm = par(m)
    //not parallel?
    val g: A => Par[B] = f andThen ((b: B) => Par.lazyUnit(b))
    foldMapV(v, parm)(g)
  }

  //answer
  // we perform the mapping and the reducing both in parallel
  //  def parFoldMap[A,B](v: IndexedSeq[A], m: Monoid[B])(f: A => B): Par[B] =
  //    Par.parMap(v)(f).flatMap { bs =>
  //      foldMapV(bs, par(m))(b => Par.asyncF(b))
  //    }

  def isOrdered(as: IndexedSeq[Int]): Boolean = {
    foldMapV(as, segmentMonoid)(a => Some((a, a, true))).forall(_._3)
  }

  def segmentMonoid: Monoid[Option[(Int, Int, Boolean)]] = new Monoid[Option[(Int, Int, Boolean)]] {
    def op(a1: Option[(Int, Int, Boolean)], a2: Option[(Int, Int, Boolean)]): Option[(Int, Int, Boolean)] =
      (a1, a2) match {
        case (a, None) => a
        case (None, a) => a
        case (Some((x1, y1, b1)), Some((x2, y2, b2))) =>
          val min = x1 min x2
          val max = y1 max y2
          val b = b1 && b2 && (y1 <= x2)
          Some((min, max, b))
      }

    def zero: Option[(Int, Int, Boolean)] = None
  }

  sealed trait WC

  case class Stub(chars: String) extends WC

  case class Part(lStub: String, words: Int, rStub: String) extends WC

  val wcMonoid: Monoid[WC] = new Monoid[WC] {
    def op(a1: WC, a2: WC): WC = (a1, a2) match {
      case (Stub(c1), Stub(c2)) => Stub(c1 + c2)
      case (Stub(c), Part(lStub, words, rStub)) => Part(c + lStub, words, rStub)
      case (Part(lStub, words, rStub), Stub(c)) => Part(lStub, words, rStub + c)
      case (Part(lStub1, words1, rStub1), Part(lStub2, words2, rStub2)) =>
        val add = if ((rStub1 + lStub2).isEmpty) 0 else 1
        Part(lStub1, words1 + words2 + add, rStub1)
    }

    def zero = Stub("")
  }

//    def wordCount: Int = (s: String) => {
//      foldMapV(s, wcMonoid)(wcCreate)
//    }

  val wcCreate: Char => WC = (c: Char) => {
    if (c.isWhitespace) Stub(c.toString)
    else Part("", 1, "")
  }

  trait Foldable[F[_]] {
    //todo: implement default methods via curried and dual
    def foldRight[A, B](as: F[A])(z: B)(f: (A, B) => B): B

    def foldLeft[A, B](as: F[A])(z: B)(f: (B, A) => B): B

    def foldMap[A, B](as: F[A])(f: A => B)(mb: Monoid[B]): B

    def concatenate[A](as: F[A])(m: Monoid[A]): A =
      foldLeft(as)(m.zero)(m.op)

    def toList[A](fa: F[A]): List[A] =
      foldLeft(fa)(List.empty[A])((list, a) => a :: list)
  }

  trait FStream extends Foldable[Stream] {
    override def foldRight[A, B](as: Stream[A])(z: B)(f: (A, B) => B): B = as match {
      case h #:: t => f(h, foldRight(t)(z)(f))
      case Empty => z
    }

    override def foldLeft[A, B](as: Stream[A])(z: B)(f: (B, A) => B): B = as match {
      case h #:: t => foldLeft(t)(f(z, h))(f)
      case Empty => z
    }

    override def foldMap[A, B](as: Stream[A])(f: A => B)(mb: Monoid[B]): B =
      foldLeft(as)(mb.zero)((b, a) => mb.op(b, f(a)))
  }

  sealed trait Tree[+A]

  case class Leaf[A](value: A) extends Tree[A]

  case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

  trait FoldableTree extends Foldable[Tree] {
    override def foldRight[A, B](as: Tree[A])(z: B)(f: (A, B) => B): B = as match {
      case Leaf(a) => f(a, z)
      case Branch(left, right) => foldRight(right)(foldRight(left)(z)(f))(f)
    }

    override def foldLeft[A, B](as: Tree[A])(z: B)(f: (B, A) => B): B = as match {
      case Leaf(a) => f(z, a)
      case Branch(left, right) => foldLeft(left)(foldLeft(right)(z)(f))(f)
    }

    override def foldMap[A, B](as: Tree[A])(f: A => B)(mb: Monoid[B]): B = as match {
      case Leaf(a) => f(a)
      case Branch(l, r) => mb.op(foldMap(l)(f)(mb), foldMap(r)(f)(mb))
    }
  }

  trait FoldableOption extends Foldable[Option] {
    override def foldRight[A, B](as: Option[A])(z: B)(f: (A, B) => B): B = as match {
      case None => z
      case Some(a) => f(a, z)
    }

    override def foldLeft[A, B](as: Option[A])(z: B)(f: (B, A) => B): B = as match {
      case None => z
      case Some(a) => f(z, a)
    }

    override def foldMap[A, B](as: Option[A])(f: A => B)(mb: Monoid[B]): B = as match {
      case None => mb.zero
      case Some(a) => f(a)
    }
  }

  def productMonoid[A, B](A: Monoid[A], B: Monoid[B]): Monoid[(A, B)] = new Monoid[(A, B)] {
    override def op(a1: (A, B), a2: (A, B)): (A, B) = {
      val (x1, y1) = a1
      val (x2, y2) = a2
      (A.op(x1, x2), B.op(y1, y2))
    }

    override def zero: (A, B) = (A.zero, B.zero)
  }

  def mapMergeMonoid[K, V](V: Monoid[V]): Monoid[Map[K, V]] =
    new Monoid[Map[K, V]] {
      def zero = Map[K, V]()

      def op(a: Map[K, V], b: Map[K, V]) =
        (a.keySet ++ b.keySet).foldLeft(zero) { (acc, k) =>
          acc.updated(k, V.op(a.getOrElse(k, V.zero),
            b.getOrElse(k, V.zero)))
        }
    }

  def functionMonoid[A, B](B: Monoid[B]): Monoid[A => B] = new Monoid[A => B] {
    override def op(a1: A => B, a2: A => B): A => B =
      (a: A) => B.op(a1(a), a2(a))

    override def zero: A => B = (a: A) => B.zero
  }

  def bag[A](as: IndexedSeq[A]): Map[A, Int] = {
    val m: Monoid[Map[A, Int]] = mapMergeMonoid(intAddition)
    foldMapV(as, m)(a => Map(a -> 1))
  }

  // We can get the dual of any monoid just by flipping the `op`.
  def dual[A](m: Monoid[A]): Monoid[A] = new Monoid[A] {
    def op(x: A, y: A): A = m.op(y, x)

    val zero = m.zero
  }

}
