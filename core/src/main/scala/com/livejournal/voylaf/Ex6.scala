package com.livejournal.voylaf

//import cats.Semigroupal
import cats.data.Validated
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.apply._
import cats.instances.list._

//import scala.util.Try
//import cats.instances.either._
import cats.syntax.either._

import language.higherKinds

object Ex6 extends App {

  import cats.Monad

  def product[M[_] : Monad, A, B](x: M[A], y: M[B]): M[(A, B)] =
    x.flatMap(a => y.map(b => (a, b)))

  //  def readName(inputMap: Map[String, String]): Either[List[String], String] = {
  //    val name = inputMap.getOrElse("name", "")
  //    if (name.isEmpty) Left(List("name must be non-empty"))
  //    else Right(name)
  //  }
  //
  //  def readAge(inputMap: Map[String, String]): Either[List[String], Int] = {
  //    val maybeAge = inputMap.get("age")
  //    maybeAge
  //      .map(_.toInt)
  //      .filter(age => (age >= 0) || (age < 150)) match {
  //      case Some(age) => Right(age)
  //      case None => Left(List("age must be greater than 0 and lesser than 150"))
  //    }
  //  }

  type FailFast[A] = Either[List[String], A]
  type FailSlow[A] = Validated[List[String], A]

  case class User(name: String, age: Int)

  def getValue(field: String)(data: Map[String, String]): FailFast[String] = {
    data.get(field).toRight(List(s"field $field is not specified"))
  }

  def getName = getValue("name") _

  def getAge = getValue("age") _

  def parseInt(field: String)(s: String): FailFast[Int] =
  //Try(s.toInt).toOption.toRight(List(s"$s must be integer"))
    Either.catchOnly[NumberFormatException](s.toInt).leftMap(_ => List(s"$field must be an integer"))

  def nonBlank(field: String)(s: String): FailFast[String] =
  //    if (s.isEmpty) Left(List("empty string"))
  //    else Right(s)
    Right(s).ensure(List(s"$field must be non-empty"))(_.nonEmpty)

  def nonNegative(field: String)(i: Int): FailFast[Int] =
  //    if (i >= 0) Right(i)
  //    else Left(List("negative number"))
    Right(i).ensure(List(s"$field must be non-negative"))(_ >= 0)

  def readName(data: Map[String, String]): FailFast[String] =
    getName(data).flatMap(nonBlank("name"))

  def readAge(data: Map[String, String]): FailFast[Int] =
    getAge(data).
      flatMap(parseInt("age")).
      flatMap(nonNegative("age"))

  def readUser(data: Map[String, String]): FailSlow[User] =
    (readName(data).toValidated, readAge(data).toValidated).mapN(User.apply)

  val test1 = readUser(Map("name" -> "Dave", "age" -> "37"))
  val test2 = readUser(Map("age" -> "-1"))
  val test = List(test1, test2)

  test.foreach(println)
}
