package com.livejournal.voylaf

object Ex1 {

  trait Printable[A] {
    def format(a: A): String
  }

  object PrintableInstances {
    implicit val stringPrintable = new Printable[String] {
      def format(a: String) = a
    }

    implicit val intPrintable = new Printable[Int] {
      def format(a: Int) = a.toString
    }

    implicit val catPrintable = new Printable[Cat] {
      def format(a: Cat) = {
        val name = Printable.format(a.name)
        val age = Printable.format(a.age)
        val color = Printable.format(a.color)
        s"$name is a $age year-old $color cat."
      }
    }
  }

  object Printable {
    def format[A](a: A)(implicit p: Printable[A]) = p.format(a)

    def print[A](a: A)(implicit p: Printable[A]) = println(format(a))
  }

  final case class Cat(name: String, age: Int, color: String)

  object PrintableSyntax {
    implicit class PrintableOps[A](value: A) {
      def format(implicit p: Printable[A]) = Printable.format(value)
      def print(implicit p: Printable[A]) = Printable.print(value)
    }
  }
}