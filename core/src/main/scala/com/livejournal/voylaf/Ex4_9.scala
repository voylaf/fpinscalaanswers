package com.livejournal.voylaf

object Ex4_9 extends App {

  import cats.data.State

  type CalcState[A] = State[List[Int], A]

  //  def evalOne(sym: String): CalcState[Int] = new CalcState[Int] {
  //    (oldStack: List[Int]) => {
  //      val newStack = symMatching(sym)(oldStack)
  //      result(sym)(newStack)
  //      //(newStack, newResult)
  //    }
  //  }
  //
  //  val numberRegex = """[0-9]""".r
  //
  //  def symMatching(sym: String)(oldStack: List[Int]) = sym match {
  //    case " " => 0 :: oldStack
  //    case i@numberRegex() => oldStack.headOption.map(_ * 10 + i.toInt).getOrElse(i.toInt) :: oldStack
  //    case _ => oldStack
  //  }
  //
  //  def result(sym: String)(stack: List[Int]): (List[Int], Int) = {
  //    sym match {
  //      case "+" => (Nil, stack.sum)
  //      case "-" => (Nil, stack.reduce(_ - _))
  //      case "*" => (Nil, stack.product)
  //      case "/" => (Nil, stack.reduce(_ / _))
  //      case _ => (stack, 0)
  //    }
  //  }

  def evalOne(sym: String): CalcState[Int] =
    sym match {
      case "+" => operator(_ + _)
      case "-" => operator(_ - _)
      case "*" => operator(_ * _)
      case "/" => operator(_ / _)
      case num => operand(num.toInt)
    }

  def operator(f: (Int, Int) => Int): CalcState[Int] =
    State[List[Int], Int] {
      case x :: y :: tail =>
        val res = f(y, x)
        (res :: tail, res)
      case _ => sys.error("fail")
    }

  def operand(i: Int): CalcState[Int] =
    State[List[Int], Int](stack =>
      (i :: stack, i)
    )

  import cats.syntax.applicative._

  def evalAll(input: List[String]): CalcState[Int] =
    input.foldLeft(0.pure[CalcState])(
      (state, str) =>
        state.flatMap(_ => evalOne(str)))

  def evalInput(str: String): Int = {
    val list = str.split(" ").toList
    evalAll(list).runA(Nil).value
  }

  val program = for {
    _ <- evalAll(List("1", "2", "+"))
    _ <- evalAll(List("3", "4", "+"))
    ans <- evalOne("*")
  } yield ans

  val result1 = evalInput("11 3 + 5 * 2 /")

  println(result1)

}
