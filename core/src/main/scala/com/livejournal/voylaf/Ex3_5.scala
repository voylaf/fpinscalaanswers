package com.livejournal.voylaf

import cats.Functor
import cats.implicits._

object Ex3_5 extends App {

  sealed trait Tree[+A]

  final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

  final case class Leaf[A](value: A) extends Tree[A]

  object Tree {
    def leaf[A](value: A): Tree[A] = Leaf(value)

    def branch[A](left: Tree[A], right: Tree[A]): Tree[A] = Branch(left, right)
  }

  implicit val treeFunctor: Functor[Tree] = new Functor[Tree] {
    def map[A, B](fa: Tree[A])(f: A => B): Tree[B] = fa match {
      case Leaf(a) => Leaf(f(a))
      case Branch(l, r) => Branch(map(l)(f), map(r)(f))
    }
  }

  val test1: Tree[Int] = Tree.branch(Tree.branch(Tree.leaf(3), Tree.leaf(5)), Tree.leaf(11))
  val testMap1 = test1.map(_ * 2)

  trait Printable[A] {
    self =>
    def format(value: A): String

    def contramap[B](func: B => A): Printable[B] =
      new Printable[B] {
        def format(value: B): String = {
          val z = self.format _ compose func
          z(value)
        }
      }
  }

  def format[A](value: A)(implicit p: Printable[A]): String =
    p.format(value)

  implicit val stringPrintable: Printable[String] =
    new Printable[String] {
      def format(value: String): String =
        "\"" + value + "\""
    }
  implicit val booleanPrintable: Printable[Boolean] =
    new Printable[Boolean] {
      def format(value: Boolean): String =
        if (value) "yes" else "no"
    }

  final case class Box[A](value: A)

  implicit def boxPrintable[A](implicit p: Printable[A]) = p.contramap((b: Box[A]) => b.value)

  println(format(Box("hello world")))
  println(format(Box(true)))

  //println(format(Box(123)))

  trait Codec[A] {
    def encode(value: A): String

    def decode(value: String): A

    def imap[B](dec: A => B, enc: B => A): Codec[B] = {
      val self = this
      new Codec[B] {
        def encode(value: B) = self.encode(enc(value))

        def decode(value: String) = dec(self.decode(value))
      }
    }
  }

  def encode[A](value: A)(implicit c: Codec[A]): String =
    c.encode(value)

  def decode[A](value: String)(implicit c: Codec[A]): A =
    c.decode(value)

  implicit val stringCodec: Codec[String] =
    new Codec[String] {
      def encode(value: String): String = value

      def decode(value: String): String = value
    }

  implicit val doubleCodec: Codec[Double] = stringCodec.imap(_.toDouble, _.toString)

}
