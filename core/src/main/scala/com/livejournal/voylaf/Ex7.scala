package com.livejournal.voylaf

object Ex7 extends App {
  val list = List(1, 2, 3)
  val t1 = list.foldLeft(List.empty[Int])((b, a) => a :: b)
  val t2 = list.foldRight(List.empty[Int])(_ :: _)
  println(t1)
  println(t2)

  def map[A, B](fa: List[A])(f: A => B) =
    fa.foldRight(List.empty[B])((a, acc) => f(a) :: acc)

  def flatMap[A, B](fa: List[A])(f: A => List[B]) =
    fa.foldRight(List.empty[B])((a, acc) => f(a) ::: acc)

  def filter[A](fa: List[A])(f: A => Boolean) =
    fa.foldRight(List.empty[A]){(a, acc) =>
      if (f(a)) a :: acc
      else acc
    }

  import scala.math.Numeric
  def sum[A](fa: List[A])(implicit numeric: Numeric[A]): A =
    fa.foldRight(numeric.zero)(numeric.plus)
}
