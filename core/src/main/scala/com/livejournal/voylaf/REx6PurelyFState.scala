package com.livejournal.voylaf

object REx6PurelyFState extends App {

  trait RNG {
    def nextInt: (Int, RNG)
  }

  case class SimpleRNG(seed: Long) extends RNG {
    def nextInt: (Int, RNG) = {
      val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
      val nextRNG = SimpleRNG(newSeed)
      val n = (newSeed >>> 16).toInt
      (n, nextRNG)
    }
  }

  //?
  def nonNegativeInt(rng: RNG): (Int, RNG) = {
    val (i, nextRNG) = rng.nextInt
    if (i == Integer.MIN_VALUE) nonNegativeInt(nextRNG)
    else (math.abs(i), nextRNG)
  }

  def intDouble(rng: RNG): ((Int, Double), RNG) = {
    val (i, nextRNG) = nonNegativeInt(rng)
    val (d, nextRNG2) = double(nextRNG)
    ((i, d), nextRNG2)
  }

  def doubleInt(rng: RNG): ((Double, Int), RNG) = {
    val (d, nextRNG) = double(rng)
    val (i, nextRNG2) = nonNegativeInt(nextRNG)
    ((d, i), nextRNG2)
  }

  def double3(rng: RNG): ((Double, Double, Double), RNG) = {
    ???
  }

  def ints(count: Int)(rng: RNG): (List[Int], RNG) = {
    def aux(n: Int)(acc: (List[Int], RNG)): (List[Int], RNG) =
      if (n == 0) {
        val (list, r) = acc
        (list.reverse, r)
      }
      else {
        val (list, r) = acc
        val (i, next) = r.nextInt
        val uacc = (i :: list, next)
        aux(n - 1)(uacc)
      }

    aux(count)((Nil, rng))
  }

  val srng = SimpleRNG(22L)
  val r1 = nonNegativeInt(srng)
  val r2 = nonNegativeEven(srng)
  val r5 = double(srng)
  val r3 = List(ints(5)(srng))

  val rr1 = List(r1, r2, r5, r3)
  rr1.foreach(println(_))

  type Rand[+A] = RNG => (A, RNG)
  val int: Rand[Int] = _.nextInt

  def unit[A](a: A): Rand[A] =
    rng => (a, rng)

  def nonNegativeEven: Rand[Int] =
    mapZ(nonNegativeInt)(i => i - i % 2)


  def double: Rand[Double] = {
    mapZ(_.nextInt)(i => i / Integer.MAX_VALUE.toDouble)
  }

  def map[A, B](s: Rand[A])(f: A => B): Rand[B] =
    rng => {
      val (a, rng2) = s(rng)
      (f(a), rng2)
    }

  def map2[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] = rng => {
    val (a, rng2) = ra(rng)
    val (b, rng3) = rb(rng2)
    (f(a, b), rng3)
  }

  def mapZ[A, B](s: Rand[A])(f: A => B): Rand[B] =
    flatMap(s)(a => (f(a), _))

  def map2Z[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] = {
    flatMap(ra)(a => flatMap(rb)(b => (f(a, b), _)))
  }

  def sequence[A](fs: List[Rand[A]]): Rand[List[A]] = {
    val r: Rand[List[A]] = unit(Nil)
    fs.foldRight(r)((ra, rl) => map2Z(ra, rl)(_ :: _))
  }

  def intsZ(count: Int): Rand[List[Int]] = {
    val list = List.fill(count)(int)
    sequence(list)
  }

  def flatMap[A, B](f: Rand[A])(g: A => Rand[B]): Rand[B] = rng => {
    val (a, ra) = f(rng)
    g(a)(ra)
  }

  val (r4, _) = intsZ(10)(srng)
  println(r4)

  /*
  todo: state exercise
   */
  case class State[S, +A](run: S => (A, S)) {

    def map[B](f: A => B): State[S, B] = State { s =>
      val (a, s2) = run(s)
      (f(a), s2)
    }

    def map2[B, C](that: State[S, B])(f: (A, B) => C): State[S, C] =
      State { s =>
        val (a, s2) = run(s)
        val (b, s3) = that.run(s2)
        (f(a, b), s3)
      }

    def flatMap[B](f: A => State[S, B]): State[S, B] =
      State { s =>
        val (a, s2) = run(s)
        val fa = f(a)
        fa.run(s2)
      }
  }

  object State {
    def unit[A, S](a: A): State[S, A] = State(s => (a, s))

    def sequence[S, A](as: List[State[S, A]]): State[S, List[A]] = {
      val r: State[S, List[A]] = State.unit(Nil)
      as.foldRight(r)((ra, rl) => ra.map2(rl)(_ :: _))
    }

    def modify[S](f: S => S): State[S, Unit] = for {
      s <- get // Gets the current state and assigns it to `s`.
      _ <- set(f(s)) // Sets the new state to `f` applied to `s`.
    } yield ()

    def get[S]: State[S, S] = State(s => (s, s))

    def set[S](s: S): State[S, Unit] = State(_ => ((), s))
  }

  type RandS[+A] = State[RNG, A]

  //todo: realize like Rand
  def nonNegativeEvenS: RandS[Int] = State { rng: RNG =>
    val (i, rng2) = nonNegativeInt(rng)
    val a: State[RNG, Int] = State.unit(i)
    a.map(i => i - i % 2).run(rng2)
  }

  def intsS(count: Int): RandS[List[Int]] =
    ???

  def doubleS: Rand[Double] = {
    mapZ(_.nextInt)(i => i / Integer.MAX_VALUE.toDouble)
  }

  val r12 = nonNegativeEven(srng)
  println(r12)

  //val (r14, _) = intsS(10).run(srng)
  //println(r14)

  /*
  Machine exercise
   */

  sealed trait Input

  case object Coin extends Input

  case object Turn extends Input

  case class Machine(locked: Boolean, candies: Int, coins: Int) {
    /*
    The method simulateMachine should operate the machine based on the list of inputs
    and return the number of coins and candies left in the machine at the end. For example,
    if the input Machine has 10 coins and 5 candies, and a total of 4 candies are successfully
    bought, the output should be (14, 1).
     */
    def simulateMachine(inputs: List[Input]): State[Machine, (Int, Int)] = {
//      val g = State.unit(xy)(this)
//
//      g(inputs.foldRight(this)((i: Input, m: Machine) => m.update(i)))
      ???
    }

    protected val Machine(_, x, y) = this
    protected val xy: (Int, Int) = (x, y)
    private val state: State[Machine, (Int, Int)] = State.unit((x, y))

    def update: Input => Machine = (input: Input) => (this, input) match {
      case (Machine(_, 0, _), _) => this
      case (Machine(true, _, _), Coin) => Machine(locked = false, candies, coins + 1)
      case (Machine(false, _, _), Turn) => Machine(locked = true, candies - 1, coins)
      case _ => this
    }

  }


}
