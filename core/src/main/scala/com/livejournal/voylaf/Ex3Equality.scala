package com.livejournal.voylaf

//import scala.annotation.tailrec

object Ex3Equality extends App {

  final case class Cat(name: String, age: Int, color: String)

  import cats.Eq
  import cats.instances.int._
  import cats.instances.string._
  import cats.instances.option._
  import cats.syntax.eq._

  implicit val catEq: Eq[Cat] = Eq.instance[Cat] {
    (cat1, cat2) => cat1.name === cat2.name && cat1.age === cat2.age && cat1.color === cat2.color
  }

  val cat1 = Cat("Garfield", 38, "orange and black")
  val cat2 = Cat("Heathcliff", 33, "orange and black")

  val optionCat1 = Option(cat1)
  val optionCat2 = Option.empty[Cat]

  println(cat1 === cat2)
  println(cat1 =!= cat2)

  println(optionCat1 === optionCat2)

  case class Left[E <: List[_]](value: E) extends Either[E, Nothing]

  case class Right[+A](value: A) extends Either[Nothing, A]

  trait Either[+E, +A] {
    def map[B](f: A => B): Either[E, B] = this match {
      case Left(e) => Left(e)
      case Right(a) => Right(f(a))
    }

    def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B] = this.map(f) match {
      case Left(e) => Left(e)
      case Right(a) => this.map(f) match {
        case Left(e) => Left(e)
        case Right(aa) => aa
      }
    }

    def orElse[EE >: E, B >: A](b: => Either[EE, B]): Either[EE, B] = this match {
      case Left(e) => b
      case Right(a) => this
    }

    def map2[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C] = for {
      aa <- this
      bb <- b
    } yield f(aa, bb)
  }

  def sequence[E, A](es: List[Either[E, A]]): Either[E, List[A]] = traverse(es)(x => x)

  def traverse[E, A, B](as: List[A])(f: A => Either[E, B]): Either[E, List[B]] = {
    val z: Either[E, List[B]] = Right(List.empty[B])
    //terminate if an error has occurred
    val g = (a: A, el: Either[E, List[B]]) => f(a).map2(el)(_ :: _)
    as.foldRight(z)(g)
  }

  /*val t1 = Right(5)
  val t2 = Right(7)
  val t3 = Right(-1)
  val r1 = t1.map2(Left("no"))((x: Int, y: Int) => x + y)
  val r2 = sequence(List(t1, t2, t3))
  val r3 = sequence(List(t1, Left("no"), t3, Left("no2")))
  val r4 = sequence(List(t1, t3))
  val r5 = sequence(List(Left("no-no")))
  //println(r1)
  List(r1, r2, r3, r4, r5) foreach println

  case class Person(name: Name, age: Age)

  sealed class Name(val value: String)

  sealed class Age(val value: Int)

  def mkName(name: String): Either[String, Name] =
    if (name == "" || name == null) Left("Name is empty.")
    else Right(new Name(name))

  def mkAge(age: Int): Either[String, Age] =
    if (age < 0) Left("Age is out of range.")
    else Right(new Age(age))

  def mkPerson(name: String, age: Int): Either[String, Person] =
    mkName(name).map2(mkAge(age))(Person(_, _))


  def sss = (i: Int) => i * 2
  def lift[A, B](f: A => B): Option[A] => Option[B] = _ map f
  def sss2 = lift(sss)
  val re = sss2(Some(3)).getOrElse(0)*/

  val list = List(1, 4, 6, 0, 7, 4)

  def safeDiv(x: Int, y: Int): Either[List[Exception], Int] =
    try Right(x / y)
    catch {
      case e: Exception => Left(e :: Nil)
    }

  val res = traverse(list)(safeDiv(10000, _))
  println(res)
}
