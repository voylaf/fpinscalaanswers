package com.livejournal.voylaf

object Ex4 extends App {

  import scala.language.higherKinds

  trait Monad[F[_]] {
    def pure[A](a: A): F[A]

    def flatMap[A, B](value: F[A])(func: A => F[B]): F[B]

    def map[A, B](value: F[A])(func: A => B): F[B] =
      flatMap(value)(a => pure(func(a)))
  }

  import cats.Id

  def pure[A](a: A): Id[A] = a

  def flatMap[A, B](a: Id[A])(func: A => Id[B]): Id[B] = func(a)

  def map[A, B](a: Id[A])(func: A => B): Id[B] = pure(func(a))

  import cats.Eval

  def foldRightEval[A, B](as: List[A], acc: Eval[B])
                         (fn: (A, Eval[B]) => Eval[B]): Eval[B] =
    as match {
      case head :: tail =>
        Eval.defer(fn(head, foldRightEval(tail, acc)(fn)))
      case Nil =>
        acc
    }

  def foldRight[A, B](as: List[A], acc: B)(fn: (A, B) => B): B =
    as match {
      case head :: tail =>
        foldRightEval(as, Eval.now(acc))((a, e) => e.map(b => fn(a, b))).value
      case Nil =>
        acc
    }

  println(foldRight((1 to 100000).toList, 0L)(_ + _))

  import scala.concurrent._
  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent.duration._

  import cats.data.Writer
  import cats.syntax.writer._

  def slowly[A](body: => A) =
    try body finally Thread.sleep(100)

  def factorial(n: Int): Int = {
    val (log, result) = factorialWriter(n).run
    println(log)
    result
  }

  def factorialWriter(n: Int): Writer[String, Int] = {
    slowly(
      if (n == 0) 1.writer(s"fact 0 1")
      else factorialWriter(n - 1).mapBoth { (log, i) =>
        val r = i * n
        (log + "\n" + s"fact $n $r", r)
      }
    )
  }

  val res = Await.result(Future.sequence(Vector(
    Future(factorial(3)),
    Future(factorial(3))
  )), 5.seconds)

  println(res)

  import cats.data.Reader
  import cats.syntax.applicative._

  case class Db(
                 usernames: Map[Int, String],
                 passwords: Map[String, String]
               )

  type DbReader[A] = Reader[Db, A]

  def findUsername(userId: Int): DbReader[Option[String]] =
    Reader(db => db.usernames.get(userId))

  def checkPassword(
                     username: String,
                     password: String): DbReader[Boolean] =
    Reader(db => db.passwords.get(username).contains(password))

  def checkLogin(
                  userId: Int,
                  password: String): DbReader[Boolean] =
    for {
      u <- findUsername(userId)
      passwordOk <- u.map(username =>
        checkPassword(username, password))
        .getOrElse(false.pure[DbReader])
    } yield passwordOk

  val users = Map(
    1 -> "dade",
    2 -> "kate",
    3 -> "margo"
  )
  val passwords = Map(
    "dade" -> "zerocool",
    "kate" -> "acidburn",
    "margo" -> "secret"
  )
  val db = Db(users, passwords)
  println(checkLogin(1, "zerocool").run(db))

  println(checkLogin(4, "davinci").run(db))

}
