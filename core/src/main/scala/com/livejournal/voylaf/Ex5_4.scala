package com.livejournal.voylaf

import cats.data.EitherT
import cats.instances.future._

import scala.concurrent.Await
import scala.concurrent.duration._
//import cats.syntax.flatMap._

import scala.concurrent.Future

object Ex5_4 extends App {

  type Response[A] = EitherT[Future, String, A]

  import scala.concurrent.ExecutionContext.Implicits.global

  def getPowerLevel(autobot: String): Response[Int] =
    powerLevels.get(autobot) match {
      case Some(i) => EitherT.right(Future(i))
      case None => EitherT.left(Future.successful(s"not info about $autobot"))
    }

  def canSpecialMove(ally1: String, ally2: String): Response[Boolean] =
    for {
      a1 <- getPowerLevel(ally1)
      a2 <- getPowerLevel(ally2)
    } yield a1 + a2 > 15

  def tacticalReport(ally1: String, ally2: String): String = {
    val inter1 = canSpecialMove(ally1, ally2)
    val inter2 = inter1.value
    val inter3 = Await.result(inter2, 5.seconds)
    inter3 match {
      case Left(str) => str
      case Right(b) if b => s"$ally1 and $ally2 can do special movie"
      case _ => s"$ally1 and $ally2 can't do special movie"
    }
  }

  val powerLevels = Map(
    "Jazz" -> 6,
    "Bumblebee" -> 8,
    "Hot Rod" -> 10
  )

  val test1 = canSpecialMove("Hot Rod", "noname")
  val test2 = canSpecialMove("Hot Rod", "Jazz")

  val test3 = tacticalReport("Jazz", "Bumblebee")
  val test4 = tacticalReport("Bumblebee", "Hot Rod")
  val test5 = tacticalReport("Jazz", "Ironhide")

  val test = List(test1, test2, test3, test4, test5)

  test.foreach(println)
}
