package com.livejournal.voylaf

//import language.higherKinds

trait JSON

object JSON {

  case object JNull extends JSON

  case class JNumber(get: Double) extends JSON

  case class JString(get: String) extends JSON

  case class JBool(get: Boolean) extends JSON

  case class JArray(get: IndexedSeq[JSON]) extends JSON

  case class JObject(get: Map[String, JSON]) extends JSON

//  def jsonParser[Parser[+ _]](P: Parsers[Parser]): Parser[JSON] = {
//    import P._
//    //    def array = wrap("[","]")(
//    //      value sep "," map (vs => JArray(vs.toIndexedSeq))) scope "array"
//
//    def literal =
//      "true".as(JBool(true)) |
//        "false".as(JBool(false)) |
//        "null".as(JNull) |
//        double.map(JNumber) |
//        stringInQuotes.map(JString)
//
//
//
//  }

}

//Example:
//{
//"Company name" : "Microsoft Corporation",
//"Ticker" : "MSFT",
//"Active" : true,
//"Price" : 30.66,
//"Shares outstanding" : 8.38e9,
//"Related companies" :
//[ "HPQ", "IBM", "YHOO", "DELL", "GOOG" ]
//}
