package com.livejournal.voylaf

import java.util.concurrent._
import java.util.concurrent.atomic.AtomicReference

import scala.concurrent.duration.TimeUnit

object REx7Parallelism extends App {

  type Par[A] = ExecutorService => Future[A]

  object Par {
    def unit[A](a: A): Par[A] = (es: ExecutorService) => UnitFuture(a)

    def lazyUnit[A](a: => A): Par[A] = fork(unit(a))

    private case class UnitFuture[A](get: A) extends Future[A] {
      def isDone = true

      def get(timeout: Long, units: TimeUnit): A = get

      def isCancelled = false

      def cancel(evenIfRunning: Boolean): Boolean = false
    }

    def map2[A, B, C](a: Par[A], b: Par[B])(f: (A, B) => C): Par[C] =
      (es: ExecutorService) => {
        val af = a(es)
        val bf = b(es)
        UnitFuture(f(af.get, bf.get))
      }

    def fork[A](a: => Par[A]): Par[A] =
      es => es.submit(new Callable[A] {
        def call = a(es).get
      })

    def asyncF[A, B](f: A => B): A => Par[B] = a => lazyUnit(f(a))

    def map[A, B](pa: Par[A])(f: A => B): Par[B] =
      map2(pa, unit(()))((a, _) => f(a))

    def sequence[A](ps: List[Par[A]]): Par[List[A]] =
      ps.foldRight(Par.unit(List.empty[A]))(map2(_, _)((a: A, b: List[A]) => a :: b))

    def parMap[A, B](ps: List[A])(f: A => B): Par[List[B]] = fork {
      val fbs: List[Par[B]] = ps.map(asyncF(f))
      sequence(fbs)
    }

    def flatMap[A, B](p: Par[A])(f: A => Par[B]): Par[B] =
      (es: ExecutorService) => {
        val pa = p(es).get
        f(pa)(es)
      }
  }

  sealed trait MyFuture[A] {
    private[voylaf] def apply(k: A => Unit): Unit
  }

  type ParNew[A] = ExecutorService => MyFuture[A]

  object ParNew {

    def unit[A](a: A): ParNew[A] =
      es => new MyFuture[A] {
        def apply(cb: A => Unit): Unit =
          cb(a)
      }

    def fork[A](a: => ParNew[A]): ParNew[A] =
      es => new MyFuture[A] {
        def apply(cb: A => Unit): Unit =
          eval(es)(a(es)(cb))
      }

    def eval(es: ExecutorService)(r: => Unit): Unit = {
      es.submit(new Callable[Unit] {
        def call = r
      })
      ()
    }

    def run[A](es: ExecutorService)(p: ParNew[A]): A = {
      val ref = new AtomicReference[A]
      val latch = new CountDownLatch(1)
      p(es) { a: A => ref.set(a); latch.countDown() }
      latch.await()
      ref.get
    }

    def choice[A](cond: ParNew[Boolean])(t: ParNew[A], f: ParNew[A]): ParNew[A] =
      es =>
        if (run(es)(cond)) t(es)
        else f(es)

    def choiceN[A](n: ParNew[Int])(choices: List[ParNew[A]]): ParNew[A] =
      (es: ExecutorService) => {
        val id = run(es)(n)
        choices(id)(es)
      }

    def choiceMap[K, V](key: ParNew[K])(choices: Map[K, ParNew[V]]): ParNew[V] =
      es => {
        val id = run(es)(key)
        choices(id)(es)
      }

    def flatMap[A, B](pa: ParNew[A])(f: A => ParNew[B]): ParNew[B] =
      es => {
        val a = run(es)(pa)
        f(a)(es)
      }

    def join[A](a: ParNew[ParNew[A]]): ParNew[A] =
      es => {
        val inner = run(es)(a)
        inner(es)
      }

    def map2[A, B, C](a: ParNew[A], b: ParNew[B])(f: (A, B) => C): ParNew[C] =
      es => {
        val ra: A = run(es)(a)
        val rb: B = run(es)(b)
        val g = (x: (A, B)) => unit(f(x._1, x._2))
        flatMap(unit((ra, rb)))(g)(es)
      }
  }

  val es = Executors.newFixedThreadPool(2)
  val r1 = (0 until 1000).toList
//  val r2 = parFilter(r1)(_ % 3 == 0)(es)
//  println(r2.get(3, TimeUnit.SECONDS))
  es.shutdown()

}
