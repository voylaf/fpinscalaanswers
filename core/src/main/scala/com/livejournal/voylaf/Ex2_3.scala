package com.livejournal.voylaf

object Ex2_3 {

  trait Semigroup[A] {
    def combine(x: A, y: A): A
  }

  trait Monoid[A] extends Semigroup[A] {
    def empty: A
  }

  object Monoid {
    def apply[A](implicit monoid: Monoid[A]) =
      monoid
  }

  implicit val BooleanAndMonoid = new Monoid[Boolean] {
    def empty = true
    
    def combine(x: Boolean, y: Boolean) = x && y
  }

  implicit val BooleanOrMonoid = new Monoid[Boolean] {
    def empty = false

    def combine(x: Boolean, y: Boolean) = x || y
  }

  implicit val BooleanXorMonoid = new Monoid[Boolean] {
    def empty = false

    def combine(x: Boolean, y: Boolean) = (x && !y) || (!x && y)
  }

  implicit val BooleanNorMonoid = new Monoid[Boolean] {
    def empty = true

    def combine(x: Boolean, y: Boolean): Boolean = (x || !y) && (!x || y)
  }

  implicit def SetMonoid[A] = new Monoid[Set[A]] {
    def empty = Set()

    def combine(x: Set[A], y: Set[A]): Set[A] = x union y
  }

}
