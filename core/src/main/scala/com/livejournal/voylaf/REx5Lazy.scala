package com.livejournal.voylaf

//import scala.collection.immutable.Stream.cons

object REx5Lazy extends App {

  sealed trait Stream[+A] {
    //infiniteStream.toList => stackOverFlow
    def toList: List[A] = aux(1)

    def aux(iter: Int): List[A] = this match {
      case Empty => Nil
      case Cons(h, t) => h() :: t().aux(iter + 1)
    }

    def take(n: Int): Stream[A] = this match {
      case Empty if n > 0 => Stream.empty[A] //throw new NoSuchElementException
      case _ if n == 0 => Stream.empty[A]
      case _ if n < 0 => throw new IllegalArgumentException(s"$n is lower than 0")
      case Cons(h, t) =>
        lazy val tail = t().take(n - 1)
        Cons(h, () => tail)
    }

    def drop(n: Int): Stream[A] = this match {
      case Empty if n > 0 => Stream.empty[A] //throw new NoSuchElementException
      case _ if n < 0 => throw new IllegalArgumentException(s"$n is lower than 0")
      case Cons(h, t) if n == 0 => this
      case Cons(h, t) => t() drop (n - 1)
    }

    def takeWhileOld(p: A => Boolean): Stream[A] = this match {
      case Empty => this
      case Cons(h, t) =>
        val head = h()
        val b = p(head)
        if (b) Cons(() => head, () => t().takeWhileOld(p))
        else Stream.empty[A]
    }

    def takeWhile(p: A => Boolean): Stream[A] =
      foldRight(Stream.empty[A]) { (a, b) =>
        val bool = p(a)
        if (bool) Cons(() => a, () => b)
        else Stream.empty[A]
      }

    def foldRight[B](z: => B)(f: (A, => B) => B): B =
      this match {
        case Cons(h, t) => f(h(), t().foldRight(z)(f))
        case _ => z
      }

    def exists(p: A => Boolean): Boolean =
      foldRight(false)((a, b) => p(a) || b)

    def forAll(p: A => Boolean): Boolean =
      foldRight(true)((a, b) => p(a) && b)

    def headOption: Option[A] =
      foldRight(None: Option[A])((a, _) => Some(a))

    def map[B](f: A => B): Stream[B] =
      foldRight(Stream.empty[B])((a, b) => Cons(() => f(a), () => b))

    def filter(p: A => Boolean): Stream[A] =
      foldRight(Stream.empty[A]) { (a, b) =>
        if (p(a)) Cons(() => a, () => b)
        else b
      }

    def append[B >: A](x: => B): Stream[B] =
      foldRight(Stream(x))((a, b) => Cons(() => a, () => b))

    def ++[B >: A](that: Stream[B]): Stream[B] =
      foldRight(that)((a, b) => Cons(() => a, () => b))

    def flatMap[B](f: A => Stream[B]): Stream[B] =
      foldRight(Stream.empty[B]) { (a, b) =>
        val x = f(a)
        x ++ b
      }

    def map2[B](f: A => B): Stream[B] =
      Stream.unfold(this) {
        case Empty => None
        case Cons(h, t) => Some((f(h()), t()))
      }

    //Stream.unfold(this.headOption)()

    def take2(n: Int): Stream[A] =
      Stream.unfold((this, n)) {
        case (Empty, _) => None
        case (_, x) if x <= 0 => None
        case (Cons(h, t), x) => Some((h(), (t(), x - 1)))
      }

    def takeWhile2(p: A => Boolean): Stream[A] =
      Stream.unfold(this) {
        case Cons(h, t) if p(h()) => Some((h(), t()))
        case _ => None
      }

    def zipWith[B](that: Stream[B]): Stream[(A, B)] =
      Stream.unfold((this, that)) {
        case (Empty, _) => None
        case (_, Empty) => None
        case (Cons(h1, t1), Cons(h2, t2)) => Some(((h1(), h2()), (t1(), t2())))
      }

    def zipAll[B](that: Stream[B]): Stream[(Option[A], Option[B])] =
      Stream.unfold((this, that)) {
        case (Empty, Cons(h, t)) => Some(((Option.empty[A], Some(h())), (Stream.empty[A], t())))
        case (Cons(h, t), Empty) => Some(((Some(h()), Option.empty[B]), (t(), Stream.empty[B])))
        case (Cons(h1, t1), Cons(h2, t2)) => Some(((Some(h1()), Some(h2())), (t1(), t2())))
        case _ => None
      }

    //it works for B == A, or else?
    def startsWith[B >: A](s: Stream[B]): Boolean = {
      val zipped = s zipAll this zipWith s
      zipped.forAll {
        case ((Some(a), Some(b)), c) => a == b && a == c
        case _ => false
      }
    }

    def tails: Stream[Stream[A]] =
      Stream.cons(this, Stream.unfold(this) {
        case Cons(h, t) => Some((t(), t()))
        case _ => None
      })

    def hasSubsequence[B >: A](s: Stream[B]): Boolean =
      tails exists (_ startsWith s)

    def scanRight[B](z: => B)(f: (A, => B) => B): Stream[B] =
      foldRight(Stream(z)) {
        case (a, b@Cons(h, t)) => Stream.cons(f(a, h()), b)
        case (a, Empty) => throw new Exception("it's a bug")
      }

    def find(p: A => Boolean): Option[A] = this match {
      case Cons(h, t) if p(h()) => Some(h())
      case Cons(h, t) if !p(h()) => t().find(p)
      case _ => None
    }
  }

  case object Empty extends Stream[Nothing]

  case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

  object Stream {
    def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
      lazy val head = hd
      lazy val tail = tl
      Cons(() => head, () => tail)
    }

    def empty[A]: Stream[A] = Empty

    def apply[A](as: A*): Stream[A] =
      if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))

    def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = {
      val opt = f(z)
      opt match {
        case None => Stream.empty[A]
        case Some((v, s)) => Stream.cons(v, unfold(s)(f))
      }
    }

    def from(n: Int): Stream[Int] =
      Stream.unfold(n)((x: Int) => Option((x, x + 1)))
  }

  def fibFrom(a: Int, b: Int): Stream[Int] = {
    println(a)
    Stream.cons(a, fibFrom(b, a + b))
  }

  //println(stream1)
  val str2 = fibs2((1, 1))
  val r1 = str2
  val r2 = str2 take2 2
  val r3 = str2 drop 1
  val r4 = str2 takeWhile2 (_ < 4)
  val r5 = str2 map2 (_ * 3)
  val r6 = str2 filter (_ % 3 == 0)
  val r7 = str2 append 147
  val r8 = str2.headOption
  val r9 = Empty.headOption
  val r10 = r2 ++ str2
  val r11 = Stream(r3 take 4, Empty, r1).flatMap(x => x)
  val res = List(r1, r2, r3, r4, r5, r6, r7, r10, r11).map(_.toList)
  println(r8)
  println(r9)
  res.foreach(println(_))

  /*
  infinite streams exercise
   */
  def constant[A](a: A): Stream[A] =
    Stream.cons(a, constant(a))

  def from(n: Int): Stream[Int] =
    Stream.cons(n, from(n + 1))

  def fibs2(state: (Int, Int)): Stream[Int] = {
    val f = (x: (Int, Int)) => x match {
      case (a, b) => Option((a + b, (b, a + b)))
    }
    Stream(state._1, state._2) ++ Stream.unfold(state)(f)
  }


  def constant2(n: Int) =
    Stream.unfold(n)((x: Int) => Option((x, x)))

  def ones2 = Stream.unfold(1)(_ => Option((1, 1)))

  val r12 = Stream.from(13)
  val r13 = constant2(5)
  val r14 = ones2
  val r15 = r12 zipWith str2
  val r16 = r12 zipAll r4
  val r17 = r1 startsWith r2
  val r18 = r1 startsWith r3
  val r19 = r4.tails
  val r22 = r1 take 8
  val r20 = r22.hasSubsequence(Stream(5, 8, 13))
  val r21 = r22.hasSubsequence(Stream(1, 2, 3, 4))
  val r23 = r22.tails
  val r24 = Stream(1, 2, 3).scanRight(0)(_ + _).toList
  List(r12, r13, r14, r15, r16).map(_.toList).foreach(println(_))
  println(List(r17, r18, r20, r21) mkString ", ")
  //r19.toList.map(_.toList).foreach(print(_))
  println(r24)
  //  println()
  //  r23.toList.map(_.toList).foreach(print(_))
}
