package com.livejournal.voylaf

object Ex2 {
  import cats.Show
  import cats.instances.int._
  import cats.instances.string._
  import cats.syntax.show._

  //implicit val intShow = Show.show(i => i.toString)
  //implicit val stringShow = Show.show(_)
  implicit val catShow = Show.show[Cat] { cat =>
    val name = cat.name.show
    val age = cat.age.show
    val color = cat.color.show
    s"$name is a $age year-old $color cat."
  }

  final case class Cat(name: String, age: Int, color: String)
}
