package com.livejournal.voylaf

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object Main {
  def main(args: Array[String]): Unit = {
    import Ex2._
    import cats.implicits._
    val cat = Cat("Bob", 10, "black")

    println(cat.show)

    def futCreate(n: Int) = Future {
      println(n)
      n
    }

    val f1 = futCreate(3)
    val f2 = futCreate(6)
    val f4 = futCreate(125)
    val f5 = futCreate(88)
    val f3 = for {
      n1 <- f1
      n2 <- f2
    } yield n1 + n2

    def sequence[T](ls: List[Future[T]]): Future[List[T]] =
      ls.foldLeft(Future(List.empty[T]))((xs, fut) => xs.flatMap(x => fut.map(y => y :: x)))
    val listf = List(f1, f2, f3, f4, f5)
    val ff = sequence(listf)
    println(Await.result(ff, 7.seconds))
  }
}
