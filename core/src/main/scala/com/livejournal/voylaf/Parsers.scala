package com.livejournal.voylaf

import language.{higherKinds, implicitConversions}
import REx8PropertyBasedTesting.{Gen, Prop}

import scala.util.matching.Regex

trait Parsers[Parser[+ _]] {
  self => // so inner classes may call methods of trait

  implicit def string(s: String): Parser[String]

  implicit def operators[A](p: Parser[A]) = ParserOps[A](p)

  implicit def regex(r: Regex): Parser[String]

  implicit def asStringParser[A](a: A)(implicit f: A => Parser[String]): ParserOps[String] = ParserOps(f(a))

  def run[A](p: Parser[A])(input: String): Either[ParseError, A]

  def char(c: Char): Parser[Char] =
    string(c.toString) map (_.charAt(0))

  def defaultSucceed[A](a: A): Parser[A] =
    string("") map (_ => a)

  def succeed[A](a: A): Parser[A]

  def many[A](p: Parser[A]): Parser[List[A]] =
    or(map2(p, many(p))(_ :: _), succeed(List()))

  def listOfN[A](n: Int, p: Parser[A]): Parser[List[A]] = {
    if (n < 1) succeed(List.empty[A])
    else map2(p, listOfN(n - 1, p))(_ :: _)
  }

  def map[A, B](a: Parser[A])(f: A => B): Parser[B] =
    for {
      aa <- a
    } yield f(aa)

  def flatMap[A, B](f: Parser[A])(g: A => Parser[B]): Parser[B]

  def slice[A](p: Parser[A]): Parser[String]

  def label[A](msg: String)(p: Parser[A]): Parser[A]

  def scope[A](msg: String)(p: Parser[A]): Parser[A]

  def attempt[A](p: Parser[A]): Parser[A]

  def many1[A](p: Parser[A]): Parser[List[A]] =
    map2(p, many(p))(_ :: _)

  def product[A, B](p: Parser[A], p2: => Parser[B]): Parser[(A, B)] =
    for {
      a <- p
      b <- p2
    } yield (a, b)

  def map2[A, B, C](p: Parser[A], p2: => Parser[B])(f: (A, B) => C): Parser[C] =
    for {
      a <- p
      b <- p2
    } yield f(a, b)

  def or[A](s1: Parser[A], s2: => Parser[A]): Parser[A]

  //json-driven methods

  def leftP[A](p1: Parser[A], p2: Parser[Any]): Parser[A] = p1

  def rightP[A](p1: Parser[Any], p2: Parser[A]): Parser[A] = p2

  def wrap[A](start: Parser[Any], stop: Parser[Any])(p: Parser[A]): Parser[A] = start ^- p -^ stop

  val doubleString = regex("[-+]?([0-9]*\\.)?[0-9]+([eE][-+]?[0-9]+)?".r)

  def double = doubleString map (_.toDouble) label ("double literal")

  def stringInQuotes = {
    val q = string("\"")
    q ^- string(".*") -^ q
  }

  case class ParserOps[A](p: Parser[A]) {
    def |[B >: A](p2: Parser[B]): Parser[B] = self.or(p, p2)

    def or[B >: A](p2: => Parser[B]): Parser[B] = self.or(p, p2)

    def run(input: String): Either[ParseError, A] = self.run(p)(input)

    def many: Parser[List[A]] = self.many(p)

    def map[B](f: A => B): Parser[B] = self.map(p)(f)

    def flatMap[B](f: A => Parser[B]): Parser[B] = self.flatMap(p)(f)

    def slice: Parser[String] = self.slice(p)

    def product[B](p2: Parser[B]): Parser[(A, B)] = self.product(p, p2)

    def **[B](p2: Parser[B]): Parser[(A, B)] = self.product(p, p2)

    def map2[B, C](p2: Parser[B])(f: (A, B) => C): Parser[C] = self.map2(p, p2)(f)

    def ^-[B](p2: Parser[B]): Parser[B] = self.rightP(p, p2)

    def -^(p2: Parser[Any]): Parser[A] = self.leftP(p, p2)

    def as[B](b: B): Parser[B] = self.map(self.slice(p))(_ => b)

    def label(s: String) = self.label(s)(p)
  }

  object Laws {
    def equal[A](p1: Parser[A], p2: Parser[A])(in: Gen[String]): Prop =
      Prop.forAll(in)(s => run(p1)(s) == run(p2)(s))

    def mapLaw[A](p: Parser[A])(in: Gen[String]): Prop =
      equal(p, p.map(a => a))(in)

    //есть ли смысл?
    def associativityProductLaw[A, B, C](p1: Parser[A], p2: Parser[B], p3: Parser[C])
                                        (in: Gen[String]): Prop = {
      val left = ((p1 ** p2) ** p3) map { case ((a, b), c) => (a, b, c) }
      val right = p1 ** (p2 ** p3) map { case (a, (b, c)) => (a, b, c) }
      equal(left, right)(in)
    }

    def mapProductLaw[A, B](p1: Parser[A], p2: Parser[A])(f: A => B)(in: Gen[String]): Prop = {
      val left = (p1 ** p2) map { case (a1, a2) => (f(a1), f(a2)) }
      val right = p1.map(f(_)) ** p2.map(f(_))
      equal(left, right)(in)
    }

  }

}

case class Location(input: String, offset: Int = 0) {

  lazy val line = input.slice(0, offset + 1).count(_ == '\n') + 1
  lazy val col = input.slice(0, offset + 1).reverse.indexOf('\n')

  def toError(msg: String): ParseError =
    ParseError(List((this, msg)))

  def advanceBy(n: Int) = copy(offset = offset + n)

  /* Returns the line corresponding to this location */
  def currentLine: String =
    if (input.length > 1) input.lines.drop(line - 1).next
    else ""
}

case class ParseError(stack: List[(Location, String)] = List(),
                      otherFailures: List[ParseError] = List()) {
  def push(loc: Location, msg: String): ParseError =
    copy(stack = (loc, msg) :: stack)

  def label[A](s: String): ParseError =
    ParseError(latestLoc.map((_, s)).toList)

  def latestLoc: Option[Location] =
    latest map (_._1)

  def latest: Option[(Location, String)] =
    stack.lastOption
}
