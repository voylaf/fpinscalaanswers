package com.livejournal.voylaf

import REx6PurelyFState._
import REx5Lazy.Stream
import REx7Parallelism.Par
import java.util.concurrent.{Executors, TimeUnit}

object REx8PropertyBasedTesting extends App {

  type FailedCase = String
  type SuccessCount = Int

  type TestCases = Int
  type MaxSize = Int

  sealed trait Result {
    def isFalsified: Boolean
  }

  case object Passed extends Result {
    def isFalsified = false
  }

  case class Falsified(failure: FailedCase,
                       successes: SuccessCount) extends Result {
    def isFalsified = true
  }

  case object Proved extends Result {
    def isFalsified: Boolean = false
  }

  case class Prop(run: (MaxSize, TestCases, RNG) => Result) {
    def &&(p: Prop): Prop = Prop {
      (max: Int, n: Int, rng: RNG) => {
        val left = this.run(max, n, rng)
        left match {
          case Passed | Proved => p.run(max, n, rng)
          case Falsified(_, _) => left
        }
      }
    }

    def ||(p: Prop): Prop = Prop {
      (max: Int, n: Int, rng: RNG) => {
        val left = this.run(max, n, rng)
        left match {
          case Falsified(_, _) => p.run(max, n, rng)
          case _ => left
        }
      }
    }
  }

  object Prop {
    def run(p: Prop,
            maxSize: Int = 100,
            testCases: Int = 100,
            rng: RNG = SimpleRNG(System.currentTimeMillis)): Unit =
      p.run(maxSize, testCases, rng) match {
        case Falsified(msg, n) =>
          println(s"! Falsified after $n passed tests:\n $msg")
        case Passed =>
          println(s"+ OK, passed $testCases tests.")
        case Proved =>
          println(s"+ OK, proved property.")
      }

    def check(p: => Boolean): Prop = Prop { (_, _, _) =>
      if (p) Proved else Falsified("()", 0)
    }

    def checkPar(p: Par[Boolean]): Prop =
      forAllPar(Gen.unit(()))(_ => p)

    val S = weighted(
      Gen.choose(1, 4).map(Executors.newFixedThreadPool) -> .75,
      Gen.unit(Executors.newCachedThreadPool) -> .25)

    def forAllPar[A](g: Gen[A])(f: A => Par[Boolean]): Prop =
      Prop.forAll(S ** g) { case s ** a =>
        val res = f(a)(s).get
        s.shutdown()
        s.awaitTermination(1, TimeUnit.MINUTES)
        res
      }

    def forAll[A](as: Gen[A])(f: A => Boolean): Prop = Prop {
      (_, n, _) =>
        //изменил rng на SimpleRNG(System.currentTimeMillis)
        val rs = randomStream(as)(SimpleRNG(System.currentTimeMillis))
        result(rs.zipWith(Stream.from(0)))(n)(f)
    }

    def forAll[A](as: Stream[A])(f: A => Boolean): Prop = Prop {
      (_, _, _) =>
        result(as.zipWith(Stream.from(0)))(Integer.MAX_VALUE)(f)
    }

    def result[A](as: Stream[(A, Int)])(n: Int)(f: A => Boolean): Result =
      as.take(n).map {
        case (a, i) => try {
          if (f(a)) Passed else Falsified(a.toString, i)
        } catch {
          case e: Exception => Falsified(buildMsg(a, e), i)
        }
      }.find(_.isFalsified).getOrElse(Passed)

    def forAll[A](g: SGen[A])(f: A => Boolean): Prop =
      forAll(g(_))(f)

    def forAll[A](g: Int => Gen[A])(f: A => Boolean): Prop = Prop {
      (max, n, rng) =>
        val casesPerSize = (n - 1) / max + 1
        val props: Stream[Prop] =
          Stream.from(0).take((n min max) + 1).map(i => Prop.forAll(g(i))(f))
        val prop: Prop =
          props.map(p => Prop { (max, n, rng) =>
            p.run(max, casesPerSize, rng)
          }).toList.reduce(_ && _)
        prop.run(max, n, rng)
    }
  }

  case class Gen[A](sample: State[RNG, A]) {
    def map[B](f: A => B): Gen[B] =
      Gen(sample.map(f))

    def flatMap[B](f: A => Gen[B]): Gen[B] =
      Gen {
        State { rng =>
          val (ra, rng2) = this.sample.run(rng)
          f(ra).sample.run(rng2)
        }
      }

    def map2[B, C](that: Gen[B])(f: (A, B) => C): Gen[C] =
      this.flatMap { a =>
        that.flatMap { b =>
          Gen.unit(f(a, b))
        }
      }

    def listOfN(size: Gen[Int]): Gen[List[A]] =
      size.flatMap { i =>
        val list = List.fill(i)(this)
        list.foldRight(Gen.unit(List.empty[A]))((a, as) => a.map2(as)(_ :: _))
      }

    def unsized: SGen[A] = SGen(i => this)

    def **[B](g: Gen[B]): Gen[(A, B)] =
      (this map2 g) ((_, _))
  }

  object Gen {
    def choose(start: Int, stopExclusive: Int): Gen[Int] =
      Gen {
        State { rng =>
          val g = (x: Int) =>
            if (start == stopExclusive) start
            else x % (stopExclusive - start) + start
          map(nonNegativeInt)(g)(rng)
        }
      }

    def unit[A](a: => A): Gen[A] = Gen(State.unit(a))
  }

  case class SGen[A](forSize: Int => Gen[A]) {
    def flatMap[B](f: A => SGen[B]): SGen[B] =
      SGen {
        i => {
          val gen = this.forSize(i)
          gen.flatMap(a => f(a).forSize(i))
        }
      }

    def apply(n: Int): Gen[A] = forSize(n)

    def map[B](f: A => B): SGen[B] =
      SGen {
        forSize(_) map f
      }
  }

  //todo:rich check
  /*
  You will need to add to the representation of `Gen`.
  For example, `Gen[Int]` should be capable of generating random integers as well
  as generating a stream of all the integers from `Int.MinValue` to `Int.MaxValue`.
  You may want to have the behavior depend on how many test cases were requested.
   */
  case class ExGen[A](forSize: Int => Either[Stream[A], Gen[A]]) {
    def apply(n: Int): Either[Stream[A], Gen[A]] = forSize(n)
  }

  object ExGen {
    def stream[A <: Ordered[A]](start: A, stopExclusive: A)(f: A => A): Stream[A] =
      Stream.unfold(start)((s: A) =>
        if ((s compare stopExclusive) >= 0) None else Some((s, f(s))))
  }

  def listOf[A](g: Gen[A]): SGen[List[A]] =
    SGen { i =>
      g.listOfN(Gen.unit(i))
    }

  def listOf1[A](g: Gen[A]): SGen[List[A]] =
    SGen { i =>
      val x =
        if (i == 0) Gen.unit(1)
        else Gen.unit(i)
      g.listOfN(x)
    }


  def randomStream[A](g: Gen[A])(rng: RNG): Stream[A] =
    Stream.unfold(rng)(r => Some(g.sample.run(r)))

  def buildMsg[A](s: A, e: Exception): String =
    s"test case: $s\n" +
      s"generated an exception: ${e.getMessage}\n" +
      s"stack trace:\n ${e.getStackTrace.mkString("\n")}"

  def unitSGen[A](a: => A): SGen[A] = SGen((i: Int) => Gen.unit(a))

  def boolean: Gen[Boolean] =
    Gen {
      State { rng =>
        map(_.nextInt)(i => i % 2 == 0)(rng)
      }
    }

  def double: Gen[Double] =
    Gen.choose(0, Int.MaxValue).flatMap(i => Gen.unit(i / Int.MaxValue.toDouble))

  def map2Old[A, B, C](a: Gen[A], b: Gen[B])(f: (A, B) => C): Gen[C] =
    Gen {
      State { rng =>
        val (ra, rng2) = a.sample.run(rng)
        val (rb, rng3) = b.sample.run(rng2)
        (f(ra, rb), rng3)
      }
    }

  def union[A](g1: Gen[A], g2: Gen[A]): Gen[A] = {
    weighted((g1, 0.5), (g2, 0.5))
  }

  def weighted[A](g1: (Gen[A], Double), g2: (Gen[A], Double)): Gen[A] =
    Gen {
      State { rng =>
        val (d, rng2) = double.sample.run(rng)
        val (ga1, d1) = g1
        val (ga2, d2) = g2
        val List(w1, w2) = List(d1, d2).map(x => x / (d1 + d2))
        val g = if (d < w1) ga1 else ga2
        g.sample.run(rng2)
      }
    }

  def equal[A](p: Par[A], p2: Par[A]): Par[Boolean] =
    Par.map2(p, p2)(_ == _)

  def option[A](a: Gen[A], b: Gen[Boolean]): Gen[Option[A]] =
    a.map2(b)((ra, rb) => if (rb) Some(ra) else None)

  val smallInt = Gen.choose(-10, 10)
  val maxProp = Prop.forAll(listOf1(smallInt)) { ns =>
    val max = ns.max
    !ns.exists(_ > max)
  }

  val sortedProp = Prop.forAll(listOf(smallInt)) { ns =>
    val z = (Integer.MAX_VALUE, true)
    ns.sorted.foldRight(z) { case (a, b) => (a, b._2 && a <= b._1) }._2
  }

  //todo: generators' bug? I changed the realization
  val takeWhileProp = Prop.forAll(listOf(smallInt)) { ns =>
    val tw = ns.takeWhile(_ < 5)
    val dw = ns.dropWhile(_ < 5)
    //    println(tw)
    //    println(dw)
    ns == tw ::: dw
  }

  object ** {
    def unapply[A, B](p: (A, B)) = Some(p)
  }

  def genStringIntFn(g: Gen[Int]): Gen[String => Int] =
    g map (i => (s => i))

  val srng = SimpleRNG(24L)
  val r1 = Gen.choose(-15, 90).sample.run(srng)
  val r2 = boolean.sample.run(srng)
  val p0 = Gen.choose(12, 20)
  val p3 = Gen.choose(1, 7).listOfN(p0)
  val p4 = boolean.listOfN(p0)
  val p6 = option(Gen.choose(1, 100), boolean).listOfN(p0)
  val p7 = Gen.choose(1, 6)
  val p8 = Gen.choose(1900, 2000)
  val p9 = union(p7, p8).listOfN(p0)
  val p10 = weighted((p7, 15), (p8, 45)).listOfN(p0)
  val p11 = listOf(p8).forSize(20)
  val r3 = p3.sample.run(srng)
  val r4 = p4.sample.run(srng)
  val r5 = p3.map2(p4)(_ zip _).sample.run(srng)
  val r6 = p6.sample.run(srng)
  val r9 = p9.sample.run(srng)
  val r10 = p10.sample.run(srng)
  val r11 = p11.sample.run(srng)
  val r8 = Prop.forAll(p8) {
    _ < 1990
  }
  val r = List(r1, r2, r3, r4, r5, r6, r9, r10, r11).map(_._1)
  r.foreach(println)
  Prop.run(r8)
  Prop.run(maxProp)
  Prop.run(sortedProp)
  Prop.run(takeWhileProp)

  val prop2 = Prop.checkPar {
    equal(
      Par.map(Par.unit(1))(_ + 1),
      Par.unit(2)
    )
  }

  Prop.run(prop2)

  def richPint(gen: Gen[Int], deep: Int): Gen[Par[Int]] = {
    if (deep <= 0) pint //gen map (Par.unit(_))
    else richPint(gen, deep - 1).map2(richPint(gen, deep - 1))((p1: Par[Int], p2: Par[Int]) => Par.fork {
      Par.map2(p1, p2)(_ + _)
    })
  }

  val pint = Gen.choose(0, 10) map (Par.unit(_))
  val prop4 =
    Prop.forAllPar(pint)(n => equal(Par.map(n)(y => y), n))

  Prop.run(prop4)

  val prop5 =
    Prop.forAllPar(pint)(n => equal(Par.fork(n), n))

  Prop.run(prop5)

}
