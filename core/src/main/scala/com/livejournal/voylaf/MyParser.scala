package com.livejournal.voylaf

import scala.util.matching.Regex
import MyParserTypes._

//class MyParser[+A]() {}

object MyParsers extends Parsers[Parser] {

  // implementations of primitives go here
  def run[A](p: Parser[A])(input: String): Either[ParseError, A] =
    p(Location(input)) match {
      case Success(a, _) => Right(a)
      case Failure(e, _) => Left(e)
    }

  def attempt[A](p: Parser[A]): Parser[A] =
    s => p(s).uncommit

  def flatMap[A, B](f: Parser[A])(g: A => Parser[B]): Parser[B] =
    s => f(s) match {
      case Success(a, n) => g(a)(s.advanceBy(n))
        .addCommit(n != 0)
        .advanceSuccess(n)
      case e@Failure(_, _) => e
    }

  def string(s: String): Parser[String] = {
    val pa = (loc: Location) => {
      if (loc.currentLine.startsWith(s)) Success(s, s.length)
      else Failure(loc.toError("Expected: " + s), isCommitted = true)
    }
    scope("An error occurred while string parsing")(pa)
  }

  def regex(r: Regex): Parser[String] =
    (loc: Location) => {
      val input = loc.currentLine
      r.findPrefixOf(input) match {
        case Some(x) => Success(x, x.length)
        case None => Failure(loc.toError("Regex: " + r), isCommitted = false)
      }
    }

  def succeed[A](a: A): Parser[A] =
    loc => Success(a, 0)

  def slice[A](p: Parser[A]): Parser[String] =
    (loc: Location) => p(loc) match {
      case Success(a, n) => Success(loc.currentLine.substring(0, n), n)
      case e@Failure(_, _) => e
    }

  def scope[A](msg: String)(p: Parser[A]): Parser[A] =
    s => p(s).mapError(_.push(s, msg))

  def label[A](msg: String)(p: Parser[A]): Parser[A] =
    s => p(s).mapError(_.label(msg))

  def or[A](x: Parser[A], y: => Parser[A]): Parser[A] =
    s => x(s) match {
      case Failure(e, false) => y(s)
      case r => r
    }
}

object MyParserTypes {

  type Parser[+A] = Location => Result[A]

  trait Result[+A] {
    def mapError(f: ParseError => ParseError): Result[A] = this match {
      case Failure(e, b) => Failure(f(e), b)
      case _ => this
    }

    def uncommit: Result[A] = this match {
      case Failure(e, true) => Failure(e, isCommitted = false)
      case _ => this
    }

    def addCommit(isCommitted: Boolean): Result[A] = this match {
      case Failure(e, c) => Failure(e, c || isCommitted)
      case _ => this
    }

    def advanceSuccess(n: Int): Result[A] = this match {
      case Success(a, m) => Success(a, n + m)
      case _ => this
    }
  }

  case class Success[+A](get: A, charsConsumed: Int) extends Result[A]

  case class Failure(get: ParseError,
                     isCommitted: Boolean) extends Result[Nothing]

}
